[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Zapouzdření*

[remark]:<slide>(new)
## Proxy
Proxy pattern využijeme, jestliže potřebujeme zajistit kontrolu nad přístupem k jinému objektu.
- Virtual proxy
- Remote proxy
- Protective proxy
- Smart proxy

[remark]:<slide>(wait)
![](media/Proxy.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface.
```java
public interface Image {
   void display();
}
```

[remark]:<slide>(wait)
Create concrete classes implementing the same interface.

```java
public class RealImage implements Image {

   private String fileName;

   public RealImage(String fileName){
      this.fileName = fileName;
      loadFromDisk(fileName);
   }

   @Override
   public void display() {
      System.out.println("Displaying " + fileName);
   }

   private void loadFromDisk(String fileName){
      System.out.println("Loading " + fileName);
   }
}
```

[remark]:<slide>(new)
Proxy implementing the same interface.

```java
public class ProxyImage implements Image{

   private RealImage realImage;
   private String fileName;

   public ProxyImage(String fileName){
      this.fileName = fileName;
   }

   @Override
   public void display() {
      if(realImage == null){
         realImage = new RealImage(fileName);
      }
      realImage.display();
   }
}
```

[remark]:<slide>(new)
Use the ProxyImage to get object of RealImage class when required.

```java
public class ProxyPatternDemo {
	
   public static void main(String[] args) {
      Image image = new ProxyImage("test_10mb.jpg");

      //image will be loaded from disk
      image.display(); 
      System.out.println("");
      
      //image will not be loaded from disk
      image.display(); 	
   }
}
```

[remark]:<slide>(new)
## Command
Zabalí metodu do objektu, takže s ní pak lze pracovat jako s běžným objektem. 

To umožňuje dynamickou výměnu požadovaných metod za běhu programu a optimalizaci přizpůsobení programu požadavkům uživatele.

[remark]:<slide>(wait)
![](media/Command.svg)

[remark]:<slide>(new)
### Příklad implementace
Create a command interface.

```java
public interface Order {
   void execute();
}
```

[remark]:<slide>(wait)
Create a request class.
```java
public class Stock {
	
   private String name = "ABC";
   private int quantity = 10;

   public void buy(){
      System.out.println("Stock [ Name: " + name + ", " +
                             "Quantity: " + quantity + " ] bought");
   }
   public void sell(){
      System.out.println("Stock [ Name: " + name + ", " + 
                             "Quantity: " + quantity + " ] sold");
   }
}
```

[remark]:<slide>(new)
Create concrete classes implementing the Order interface.

```java
public class BuyStock implements Order {
   private Stock abcStock;

   public BuyStock(Stock abcStock){
      this.abcStock = abcStock;
   }

   public void execute() {
      abcStock.buy();
   }
}
```

```java
public class SellStock implements Order {
   private Stock abcStock;

   public SellStock(Stock abcStock){
      this.abcStock = abcStock;
   }

   public void execute() {
      abcStock.sell();
   }
}
```

[remark]:<slide>(new)
Create command invoker class.

```java
public class Broker {
   private List<Order> orderList = new ArrayList<Order>(); 

   public void takeOrder(Order order){
      orderList.add(order);		
   }

   public void placeOrders(){
   
      for (Order order : orderList) {
         order.execute();
      }
      orderList.clear();
   }
}
```

[remark]:<slide>(new)

Use the Broker class to take and execute commands.

```java
public class CommandPatternDemo {
   public static void main(String[] args) {
      Stock abcStock = new Stock();

      BuyStock buyStockOrder = new BuyStock(abcStock);
      SellStock sellStockOrder = new SellStock(abcStock);

      Broker broker = new Broker();
      broker.takeOrder(buyStockOrder);
      broker.takeOrder(sellStockOrder);

      broker.placeOrders();
   }
}
```

[remark]:<slide>(new)
## Iterator
Řeší problém, jak se pohybovat mezi prvky, které jsou sekvenčně uspořádány, bez znalosti implementace jednotlivých prvků posloupnosti.

[remark]:<slide>(wait)
![](media/Iterator.svg)

[remark]:<slide>(new)
### Příklad implementace
Create interfaces.

```java
public interface Iterator {
   public boolean hasNext();
   public Object next();
}
```

```java
public interface Container {
   public Iterator getIterator();
}
```

[remark]:<slide>(new)
Create concrete class implementing the Container interface. This class has inner class NameIterator implementing the Iterator interface.

```java
public class NameRepository implements Container {
   public String names[] = {"Robert" , "John" ,"Julie" , "Lora"};

   @Override
   public Iterator getIterator() {
      return new NameIterator();
   }

   private class NameIterator implements Iterator {

      int index;

      @Override
      public boolean hasNext() {
         return index < names.length;
      }

      @Override
      public Object next() {
         if(this.hasNext()){
            return names[index++];
         }
         return null;
      }		
   }
}
```

[remark]:<slide>(new)
Use the NameRepository to get iterator and print names.

```java
public class IteratorPatternDemo {
	
   public static void main(String[] args) {
      NameRepository namesRepository = new NameRepository();

      for(Iterator iter = namesRepository.getIterator(); iter.hasNext();){
         String name = (String)iter.next();
         System.out.println("Name : " + name);
      } 	
   }
}
```

[remark]:<slide>(new)
## State
Řeší výrazný rozdíl mezi chováním objektů v různých stavech zavedením vnitřního stavu jako objektu reprezentovaného instancí některého ze stavových tříd. 

Změna stavu objektu se pak řeší záměnou objektu reprezentujího stav.

[remark]:<slide>(wait)
![](media/State.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface.

```java
public interface State {
   public void doAction(Context context);
}
```

[remark]:<slide>(wait)
Create Context Class.

```java
public class Context {
   private State state;

   public Context(){
      state = null;
   }

   public void setState(State state){
      this.state = state;		
   }

   public State getState(){
      return state;
   }
}

```
[remark]:<slide>(new)
Create concrete classes implementing the same interface.

```java
public class StartState implements State {

   public void doAction(Context context) {
      System.out.println("Player is in start state");
      context.setState(this);	
   }

   public String toString(){
      return "Start State";
   }
}
```

```java
public class StopState implements State {

   public void doAction(Context context) {
      System.out.println("Player is in stop state");
      context.setState(this);	
   }

   public String toString(){
      return "Stop State";
   }
}
```

[remark]:<slide>(new)
Use the Context to see change in behaviour when State changes.

```java
public class StatePatternDemo {
   public static void main(String[] args) {
      Context context = new Context();

      StartState startState = new StartState();
      startState.doAction(context);

      System.out.println(context.getState().toString());

      StopState stopState = new StopState();
      stopState.doAction(context);

      System.out.println(context.getState().toString());
   }
}
```

[remark]:<slide>(new)
## Template Method
Definuje metodu obsahující kostru nějakého algoritmu. 

Ne všechny kroky tohoto algoritmu jsou však známi v době vzniku této šablony. Jejich konkrétní implementaci definují až potomci.

[remark]:<slide>(wait)
![](media/Template_method.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an abstract class with a template method being final.

```java
public abstract class Game {
   abstract void initialize();
   abstract void startPlay();
   abstract void endPlay();

   //template method
   public final void play(){

      //initialize the game
      initialize();

      //start game
      startPlay();

      //end game
      endPlay();
   }
}
```

[remark]:<slide>(new)
Create concrete classes extending the above class.

```java
public class Cricket extends Game {

   @Override
   void endPlay() {
      System.out.println("Cricket Game Finished!");
   }

   @Override
   void initialize() {
      System.out.println("Cricket Game Initialized! Start playing.");
   }

   @Override
   void startPlay() {
      System.out.println("Cricket Game Started. Enjoy the game!");
   }
}
```

```java
public class Football extends Game {

   @Override
   void endPlay() {
      System.out.println("Football Game Finished!");
   }

   @Override
   void initialize() {
      System.out.println("Football Game Initialized! Start playing.");
   }

   @Override
   void startPlay() {
      System.out.println("Football Game Started. Enjoy the game!");
   }
}
```

[remark]:<slide>(new)
Use the Game's template method play() to demonstrate a defined way of playing game.

```java
public class TemplatePatternDemo {
   public static void main(String[] args) {

      Game game = new Cricket();
      game.play();
      System.out.println();
      game = new Football();
      game.play();		
   }
}
```