package cz.ictpro.tieto.state;

public class LoggerDecoratorAcounts extends DecoratorAcounts {

    public LoggerDecoratorAcounts(Acounts decoratedAcounts) {
        super(decoratedAcounts);
    }

    @Override
    public void putMoney(long amount) {
        System.out.printf("putMoney(%d)%n", amount);
        super.putMoney(amount);
    }

    @Override
    public void drawMoney(long amount) {
        System.out.printf("takeLoan(%d)%n", amount);
        super.drawMoney(amount);
    }

    @Override
    public void takeLoan(long amount) {
        System.out.printf("takeLoan(%d)%n", amount);
        super.takeLoan(amount);
    }
}
