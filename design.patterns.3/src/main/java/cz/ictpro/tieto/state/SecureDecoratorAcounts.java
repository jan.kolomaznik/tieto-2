package cz.ictpro.tieto.state;

public class SecureDecoratorAcounts extends DecoratorAcounts {

    public SecureDecoratorAcounts(Acounts decoratedAcounts) {
        super(decoratedAcounts);
    }

    @Override
    public void putMoney(long amount) {
        if (amount > 1_000_000) {
            System.out.printf("Big incoming %d%n", amount);
        }
        super.putMoney(amount);
    }

    @Override
    public void drawMoney(long amount) {
        if (amount > 1_000_000) {
            System.out.printf("Big outcome %d%n", amount);
        }
        super.drawMoney(amount);
    }

    @Override
    public void takeLoan(long amount) {
        if (amount > 1_000_000) {
            System.out.printf("Big loan %d%n", amount);
        }
        super.takeLoan(amount);
    }
}
