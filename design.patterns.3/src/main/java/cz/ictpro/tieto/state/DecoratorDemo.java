package cz.ictpro.tieto.state;

public class DecoratorDemo {

    public static void main(String[] args) {

        Acounts acounts = new Acounts();
        acounts = new LoggerDecoratorAcounts(acounts);
        acounts = new SecureDecoratorAcounts(acounts);

        acounts.putMoney(10_000);
        acounts.putMoney(10_000_000);
    }
}
