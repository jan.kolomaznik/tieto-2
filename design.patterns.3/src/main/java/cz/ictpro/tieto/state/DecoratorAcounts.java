package cz.ictpro.tieto.state;

public abstract class DecoratorAcounts extends Acounts {

    private final Acounts decoratedAcounts;

    public DecoratorAcounts(Acounts decoratedAcounts) {
        this.decoratedAcounts = decoratedAcounts;
    }

    @Override
    public void putMoney(long amount) {
        decoratedAcounts.putMoney(amount);
    }

    @Override
    public void drawMoney(long amount) {
        decoratedAcounts.drawMoney(amount);
    }

    @Override
    public void takeLoan(long amount) {
        decoratedAcounts.takeLoan(amount);
    }
}
