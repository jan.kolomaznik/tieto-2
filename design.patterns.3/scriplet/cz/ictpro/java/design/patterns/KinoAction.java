package cz.ictpro.java.design.patterns;

public class KinoAction implements ActionCreator{

    public KinoAction() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String execute() { ;
        return "Kino";
    }
}
