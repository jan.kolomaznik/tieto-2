package cz.mendelu.pjj.robot;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Coordinate {

    private static Map<String, Coordinate> coordinateMap = new HashMap<>();

    public static Coordinate coordinate(int x, int y) {
        String key = x + ":" + y;
        if (!coordinateMap.containsKey(key)) {
            coordinateMap.put(key, new Coordinate(x, y));
        }
        return coordinateMap.get(key);
    }

    private final int x, y;

    private Coordinate(int x, int y) {
        System.out.printf("Create coordinate [%d, %d]%n", x, y);
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
