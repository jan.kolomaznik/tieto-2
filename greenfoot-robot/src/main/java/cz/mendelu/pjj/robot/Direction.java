package cz.mendelu.pjj.robot;

/**
 * Created by Honza on 09.11.2016.
 */
public enum Direction {

    NORTH     ( 0, -1),
    NORTH_EAST( 1, -1),
    EAST      ( 1,  0),
    SOUTH_EAST( 1,  1),
    SOUTH     ( 0,  1),
    WEST      (-1,  0);

    private static final int LENGTH = values().length;

    private final int dx, dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public Direction onLeft() {
        int index = (ordinal() + 1) % LENGTH;
        return values()[index];
    }

    public Direction onRight() {
        int index = (LENGTH + ordinal() - 1) % LENGTH;
        return values()[index];
    }

    public Coordinate forward(Coordinate coordinate) {
        int x = coordinate.getX() + dx;
        int y = coordinate.getY() + dy;
        return Coordinate.coordinate(x, y);
    }

}
