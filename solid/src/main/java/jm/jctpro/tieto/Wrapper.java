package jm.jctpro.tieto;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Stream;

public class Wrapper {

    static class MyFile {

        private final File file;

        public MyFile(String path) {
            this.file = new File(path);
        }

        public String getName() {
            return file.getName();
        }

        public String[] list() {
            return file.list();
        }

        public Stream<String> stream() {
            return Arrays.stream(file.list());
        }
    }


    public static void main(String[] args) {
        MyFile file = new MyFile(".");
        file.stream().forEach(System.out::println);
    }
}
