package jm.jctpro.tieto;

import java.awt.*;

/**
 * SOLID principles: responsibility
 */
public class Solid_S_Solution {

    static public class Dot {

        private int x, y;

        private Color color;

        public Dot(int x, int y, Color color) {
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Color getColor() {
            return color;
        }

    }

    static class Canvas {

        public void draw(Dot dot) {
            // TODO
        }

    }

    static class Printer {

        public void print(Dot dot) {
            // TODO
        }
    }
}
