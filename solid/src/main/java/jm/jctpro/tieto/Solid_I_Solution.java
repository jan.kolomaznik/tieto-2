package jm.jctpro.tieto;

import java.awt.*;

public class Solid_I_Solution {

    interface Drawable {

        void draw();
    }

    interface Printable {

        void print();
    }

    static public class Dot implements Drawable, Printable {

        private int x, y;

        private Color color;

        public Dot(int x, int y, Color color) {
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Color getColor() {
            return color;
        }

        public void draw() {
            // TODO
        }

        public void print() {
            // TODO
        }
    }
}
