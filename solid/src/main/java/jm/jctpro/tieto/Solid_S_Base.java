package jm.jctpro.tieto;

import java.awt.*;

/**
 * SOLID principles: responsibility
 */
public class Solid_S_Base {

    static public class Dot {

        private int x, y;

        private Color color;

        public Dot(int x, int y, Color color) {
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Color getColor() {
            return color;
        }

        public void draw() {
            // TODO
        }

        public void print() {
            // TODO
        }
    }
}
