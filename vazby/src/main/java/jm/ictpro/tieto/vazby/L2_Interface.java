package jm.ictpro.tieto.vazby;

public class L2_Interface {

    static interface Motor {

    }

    static class Auto_1 {

        private final Motor motor;

        public Auto_1(Motor motor) {
            this.motor = motor;
        }
    }

    static class Auto_2 {

        private Motor motor;

        public void setMotor(Motor motor) {
            this.motor = motor;
        }
    }
}
