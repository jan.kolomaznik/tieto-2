package jm.ictpro.tieto.vazby;

public class L3_Aggreation {

    static class Motor {

    }

    static class Auto_1 {

        private final Motor motor;

        public Auto_1(Motor motor) {
            this.motor = motor;
        }
    }

    static class Auto_2 {

        private Motor motor;

        public void setMotor(Motor motor) {
            this.motor = motor;
        }
    }
}
