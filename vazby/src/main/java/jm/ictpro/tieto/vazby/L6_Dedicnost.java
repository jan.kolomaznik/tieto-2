package jm.ictpro.tieto.vazby;

public class L6_Dedicnost {

    static class Zvire {

        public void sound() {

        }
    }

    /**
     * Potomek MUSI BYT pouzitelny vsude, kde se pouziva predek
     */
    static class Pes extends Zvire  {

        @Override
        public void sound() {
            super.sound();
        }
    }
}
