package jm.java.collections;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Predmet {

    private final String kod;

    private List<Cviceni> cviceniList = new ArrayList<>();

    private SortedMap<Student, Cviceni> studentMap = new TreeMap<>();

    public Predmet(String kod) {
        this.kod = kod;
    }

    public Cviceni zalozCviceni(int kapacita) {
        Cviceni cviceni = new Cviceni(kapacita, this);
        cviceniList.add(cviceni);
        return cviceni;
    }

    public Collection<Cviceni> getCvicenis() {
        return Collections.unmodifiableCollection(cviceniList);
    }

    public Cviceni getCviceni(int index) {
        return cviceniList.get(index);
    }

    public Collection<Student> getStudentyBezCviceni() {
//        SortedSet result = new TreeSet();
//        for (Map.Entry entry: studentMap.entrySet()) {
//             if (entry.getValue() == null) {
//                 result.add(entry.getKey());
//             }
//        }
//        return result;
        return studentMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == null)
                .map(entry -> entry.getKey())
                .collect(Collectors.toSet());
    }

    public boolean zapisDoPredmetu(Student student) {
        if (studentMap.containsKey(student)) {
            return false;
        }

        studentMap.put(student, null);
        return true;
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(studentMap.keySet());
    }

    void priradStudentaDoCviceni(Student student, Cviceni cviceni) {
        studentMap.put(student, cviceni);
    }

    @Override
    public String toString() {
        return "Predmet{" +
                "kod='" + kod + '\'' +
                ", cviceniList=" + cviceniList +
                ", studentMap=" + studentMap +
                '}';
    }

    public int getKapacitaPredmetu() {
//        int result = 0;
//        for (Cviceni cviceni: cviceniList) {
//            result += cviceni.getKapacita();
//        }
//        return result;
        return cviceniList.stream()
                .mapToInt(Cviceni::getKapacita)
                //.mapToInt(c -> c.getKapacita())
                /*.mapToInt(c -> {
                    return c.getKapacita();
                })*/
                //.mapToInt(this::kapacitaCviceni)
                //.map(Cviceni::getKapacita)
                .sum();
    }

    private int kapacitaCviceni(Cviceni c) {
        return c.getKapacita();
    }
}
