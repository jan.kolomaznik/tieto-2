package jm.java.threads.lesson;

public class Thread_join extends Thread {

    private int value = -1;

    public Thread_join(String jmeno) {
        super(jmeno);
    }

    public void run() {
        for (int i = 1; i <= 3; i++) {
            System.out.println(i + ". " + getName());
            value = i;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Jsem vzhuru - " + getName());
            }
        }
    }

    public static void main(String[] args) {
        Thread_join vl = new Thread_join("Pepa");

        Thread startStop = new Thread(() -> {
            System.out.println("Start");
            vl.start();
            try {
                vl.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Stop");
        });
        startStop.start();
    }
}