package jm.java.threads.lesson;

import java.util.concurrent.atomic.AtomicLong;

public class Thread_interrupt {

    static class ThreadStopInCouting extends Thread {

        private AtomicLong aLong = new AtomicLong();

        @Override
        public void run() {
            while (!isInterrupted()) {
                aLong.incrementAndGet();
            }
        }
    }

    static class ThreadStopInWaiting extends Thread {

        private AtomicLong aLong = new AtomicLong();

        @Override
        public void run() {
            try {
                while (true) {
                    System.out.println("Zacinam cekat.");
                    Thread.sleep(60_000_000);
                }
            } catch (InterruptedException e) {
                System.out.println("Jsem presuseno.");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadStopInCouting couting = new ThreadStopInCouting();
        couting.start();
        Thread.sleep(1000);
        couting.interrupt();
        System.out.println(couting.aLong);
        System.out.printf("Is counting alive: %s%n", couting.isAlive());

        ThreadStopInWaiting waiting = new ThreadStopInWaiting();
        waiting.start();
        Thread.sleep(1000);
        waiting.interrupt();
        System.out.printf("Is waiting alive: %s%n", waiting.isAlive());
    }
}
