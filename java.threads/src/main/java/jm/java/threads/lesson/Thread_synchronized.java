package jm.java.threads.lesson;

import java.util.Arrays;
import java.util.Random;

public class Thread_synchronized {

    private int x,y;

    public Thread_synchronized(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int[] get() {
        //synchronized (this) {
            System.out.println("get: " + Thread.currentThread().getName());
            return new int[]{x, y};
        //}
    }

    public void set(int x, int y) {
        //synchronized (this) {
            System.out.println("set: " + Thread.currentThread().getName());
            this.x = x;
            this.y = y;
        //}
    }

    public static void main(String[] args) {
        Random random = new Random();
        Thread_synchronized p = new Thread_synchronized(0,0);
        Runnable r = () -> {
            String name = Thread.currentThread().getName();
            while (true) {
                int x = random.nextInt(10);
                int y = random.nextInt(10);

                synchronized (p) {
                    p.set(x, y);
                    Thread.yield();
                    int[] xy = p.get();
                    System.out.printf("%s: [%d, %d] -> %s%n", name, x, y, Arrays.toString(xy));
                }
            }
        };

        new Thread(r, "Vlakni 1").start();
        new Thread(r, "Vlakni 2").start();
    }
}
