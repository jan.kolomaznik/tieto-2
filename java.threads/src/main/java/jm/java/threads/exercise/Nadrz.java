package jm.java.threads.exercise;

public class Nadrz {

    private long obsah = 1_000_000;

    public long getObsah() {
            return obsah;
    }

    public void inc() {
            this.obsah++;
    }

    public void dec() {
            this.obsah--;
    }
}
