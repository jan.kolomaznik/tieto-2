package jm.java.streams.lesson;

import java.util.Date;
import java.util.function.Function;

public class Main {
    interface MyFoo {
        double doit(long l, int i);
    }

    static class MyFooImpl implements MyFoo {

        @Override
        public double doit(long l, int i) {
            return Math.pow(l, i);
        }
    }

    public static void main(String[] args) {
        Function<Long, Date> generatorData2 = l -> new Date();


        Function<Long, Date> generatorData = Date::new;
        Date datum = generatorData.apply(System.currentTimeMillis());
        System.out.println(datum);

        MyFoo foo = (a, b) -> a * b;
        foo.doit(1, 2);

        MyFoo pow = new MyFooImpl();
        pow.doit(1, 2);
    }
}
