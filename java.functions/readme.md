[remark]:<class>(center, middle)
# Java functions a default methods

[remark]:<slide>(new)
## Funkční rozhraní
Rozhraní deklarující právě jednu abstraktní metodu (tj. vyžadující implementaci právě jedné metody).

Ve standardní knihovně taková rozhraní již byla, ale doposud byla především jednoúčelová (`Comparable`, `Runnable`)

Knihovna Javy 8 přináší balíček `java.util.function`, definující celou řadu obecných funkčních rozhraní.

[remark]:<slide>(new)
### Funkční rozhraní: Customer
Metody rozhraní ze skupiny `Consumer` jsou čistí konzumenti. 

Zpracují své parametry, ale nic nevrací.

```java
Consumer<T>       void accept(T value)
DoubleConsumer    void accept(double value)
IntConsumer       void accept(int value)
LongConsumer      void accept(long value)
BiConsumer<T, U>  void accept(T t, U value)
ObjDoubleConsumer void accept(T t, double value)
ObjIntConsumer    void accept(T t, int value)
ObjLongConsumer   void accept(T t, long value)
```

[remark]:<slide>(new)
### Funkční rozhraní: Supplier
Metody rozhraní ze skupiny `Supplier` jsou naopak čistí producenti. 

Nemají parametry a vracejí hodnotu zadaného typu.

```java
Supplier<T>     T       get()
BooleanSupplier boolean getAsBoolean()
DoubleSupplier  double  getAsDouble()
IntSupplier     int     getAsInt()
LongSupplier    long    getAsLong()
```

[remark]:<slide>(new)
### Funkční rozhraní: Function
Metody rozhraní ze skupiny `Function` jsou klasické funkce: zpracují svůj parametr a vrátí funkční hodnotu.

```java
Function<T, R>    R apply(T value)
DoubleFunction<R> R apply(double value)
IntFunction<R>    R apply(int value)
LongFunction<R>   R apply(long value)
BiFunction<T,U,R> R apply(T t, U u)
```
A další varianty
```java
ToIntFunction<T>          int    applyAsInt(T value)
ToDoubleFunction<T>       double applyAsDouble(T value)
ToLongFunction<T>         long   applyAsLong(T value)
DoubleToIntFunction       int    applyAsInt(double value)
DoubleToLongFunction      long   applyAsLong(double value)
IntToDoubleFunction       double applyAsDouble(int value)
IntToLongFunction         long   applyAsLong(int value)
LongToDoubleFunction      double applyAsDouble(long value)
LongToIntFunction         int    applyAsInt (long value)
ToDoubleBiFunction<T,U,R> double applyAsDouble(T t,U u)
ToIntBiFunction <T,U,R>   int    applyAsInt (T t, U u)
ToLongBiFunction <T,U,R>  long   applyAsLong (T t, U u)
```

[remark]:<slide>(new)
### Funkční rozhraní: UnaryOperator
Metody rozhraní ze skupiny `UnaryOperator` představují unární operátory.

Jejich návratová hodnota je stejného typu jako jejich parametr.

```java
UnaryOperator<T>     T      apply (T operand)
DoubleUnaryOperator  double applyAsDouble(double operand)
IntUnaryOperator     int    applyAsInt(int operand)
LongUnaryOperator    long   applyAsLong(long operand)
```

[remark]:<slide>(new)
### Funkční rozhraní: BinaryOperator
Metody rozhraní ze skupiny `BinaryOperator` představují binární operátory. 

Mají dva parametry, přičemž typy obou parametrů i funkční hodnoty jsou shodné.

```java
BinaryOperator<T>    T      apply (T left, T right)
DoubleBinaryOperator double applyAsDouble(double left, double right)
IntBinaryOperator    int    applyAsInt (int left, int right)
LongBinaryOperator   long   applyAsLong (long left, long right)
```

[remark]:<slide>(new)
### Funkční rozhraní: BinaryOperator
Predikát je zde funkce, jejímž oborem hodnot je boolean. 

Definiční obor je daný generickým typem a může to být cokoli. 

Pomocí metody `Predicate.test(T t)` vyhodnocujeme pravdivost predikátu nad zadanou hodnotu.
```java
Predicate<T>      boolean test(T value)
DoublePredicate   boolean test(double value)
IntPredicate      boolean test(int value)
LongPredicate     boolean test(long value)
BiPredicate<T,U>  boolean test(T t, U u)
```

[remark]:<slide>(new)
### Funkční rozhraní: @FunctionalInterface

Označíme-li rozhraní touto anotací, bude překladač kontrolovat, zda toto rozhraní obsahuje opravdu právě jednu abstraktní metodu.

Neabstraktních metod může mít libovolné množství.

#### Jaké další interface znáte?
[remark]:<slide>(wait)
```java
java.lang.Runnable
java.lang.Iterable<T>
java.lang.Comparable<T>
java.util.concurrent.Callable<V>
java.awt.event.ActionListener
```

[remark]:<slide>(new)
## Default metody

V Javě 8 můžou rozhraní obsahovat i neabstraktní metody, tedy metody, obsahující implementaci, ne jen hlavičku. 

Pro označení těchto metod se používá klíčové slovo default.

Třída, která implementuje toto rozhraní, tak získá i tuto funkcionalitu – jedná se tak v podstatě o vícenásobnou dědičnost. 

Užitečné je to mj. v souvislosti s funkčními rozhraními a lambdami. 

Pomocí lambda výrazu definujeme určitou funkcionalitu, ale koncový uživatel daného rozhraní může volat jinou metodu (výchozí, definovanou v rozhraní), která tu metodu z lambdy obaluje, případně dělá něco úplně jiného (např. skládá více funkčních rozhraní dohromady – viz dále).

[remark]:<slide>(new)
### Příklad: Function
Ukázky výchozích metod:

```java
IntUnaryOperator prictiJedna = new IntUnaryOperator() {
    @Override
    public int applyAsInt(int a) {
        return a + 1;
    }
};

IntUnaryOperator vynasobDvema = new IntUnaryOperator() {
    @Override
    public int applyAsInt(int a) {
        return a * 2;
    }
};

int x = vynasobDvema.compose(prictiJedna).apply(5); // (5 + 1) × 2 = 12
int y = prictiJedna.compose(vynasobDvema).apply(5); // (5 × 2) + 1 = 11
int z = vynasobDvema.andThen(prictiJedna).apply(5); // (5 × 2) + 1 = 11
```

[remark]:<slide>(new)
### Příklad: Vytvoření predikátů

```java
Predicate<String> neniNull = new Predicate<String>() {
    @Override
    public boolean test(String s) {
        return Objects.nonNull(s);
    }
};
Predicate<String> spravnaDelka = new Predicate<String>() {
    @Override
    public boolean test(String s) {
        return s.matches(".{8,32}");
    }
};
Predicate<String> obsahujeCislici = new Predicate<String>() {
    @Override
    public boolean test(String s) {
        return s.matches(".*[0-9].*");
    }
};
Predicate<String> obsahujeVelkaPismeno = new Predicate<String>() {
    @Override
    public boolean test(String s) {
        return s.matches(".*[A-Z].*");
    }
};
```

[remark]:<slide>(new)
### Příklad: Použití predikátů

```java
neniNull.test("abc");            // vrátí true
neniNull.test(null);             // vrátí false
spravnaDelka.test("abcdefghij"); // vrátí true
spravnaDelka.test("abc");        // vrátí false
obsahujeCislici.test("ab7c"));   // vrátí true
obsahujeCislici.test("abc"));    // vrátí false
```

[remark]:<slide>(new)
### Příklad: Použití predikátů 2
Stejně jako funkce – i predikáty můžeme díky výchozím metodám skládat:

```java
Predicate<String> slozenyPredikat = neniNull
        .and(spravnaDelka)
        .and(obsahujeCislici.or(obsahujeVelkaPismeno));
```

a složený predikát vyhodnotit:

```java
boolean a = slozenyPredikat.test("Abcd123");     // = false
boolean b = slozenyPredikat.test("abcdefgh");    // = false
boolean c = slozenyPredikat.test("abcdefgh123"); // = true
boolean d = slozenyPredikat.test("Abcdefgh");    // = true
```

[remark]:<slide>(new)
[remark]:<class>(center, middle)
# Cvičení

Projděte příklady:

[DefaultMethods_Intro.java](src/main/java/jm/java/functions/lesson/DefaultMethods_Intro.java)

[DefaultMethods_More.java](src/main/java/jm/java/functions/lesson/DefaultMethods_More.java)