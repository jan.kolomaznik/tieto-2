package tieto;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

public class ClusterGenerator {

    private String SWITCH_ID = "swt-%d";

    private Function<Integer, Cluster.ClusterType> clusterType;

    public List<Cluster> generate(int clusters, int logicalPorts) {
        List<Cluster> result = new ArrayList<>();
        int nextSwitch = 1;

        for (int c = 0; c < clusters; c++) {
            String swt_a = format(SWITCH_ID, nextSwitch++);
            String swt_b = format(SWITCH_ID, nextSwitch++);
            Cluster cluster = new Cluster(clusterType.apply(c), swt_a, swt_b);
            for (int lp = 0; lp < ; lp++) {

            }
            result.add(new Cluster(Cluster.ClusterType.GREEN, "swt1", "swt2")
                    .addLogicalPort("lpG1", new LogicalPort("lpG1", LogicalPort.LogicalPortType.CLUSTER)
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-a")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-b")
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-c_null")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-d"))
                    .addLogicalPort("lpG2", new LogicalPort("lpG2", LogicalPort.LogicalPortType.LOOPBACK)
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-a")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-b_null")
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-c")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-d"))
                    .addLogicalPort("lpG3", new LogicalPort("lpG3", LogicalPort.LogicalPortType.LOOPBACK)
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-a_null")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-b")
                            .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-c_null")
                            .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-d"))
        );
        }
        return result;
    }
}
