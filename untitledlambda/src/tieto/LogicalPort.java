package tieto;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LogicalPort {
    
    public enum LogicalPortType {
        CLUSTER, LOOPBACK;
    }
    
    private String uuid;
    // String of the PhysicalSwitch - list portov
    private ConcurrentMap<String, List<String>> physicalSwitchPhysicalPorts = new ConcurrentHashMap<>();
    private LogicalPortType portType;
    
    public LogicalPort(String uuid, LogicalPortType portType) {
        this.uuid = uuid;
        this.portType = portType;
    }
    
    public LogicalPort addPhysicalSwitchPhysicalPort(String uuid, String port) {
        if (!physicalSwitchPhysicalPorts.containsKey(uuid)) {
            physicalSwitchPhysicalPorts.put(uuid, new LinkedList<>());
        }
        List<String> potrs = physicalSwitchPhysicalPorts.get(uuid);
        potrs.add(port);
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public ConcurrentMap<String, List<String>> getPhysicalSwitchPhysicalPorts() {
        return physicalSwitchPhysicalPorts;
    }

    public LogicalPortType getPortType() {
        return portType;
    }

    @Override
    public String toString() {
        return "LogicalPort{" +
                "uuid='" + uuid + '\'' +
                ", portType=" + portType +
                '}';
    }
}
