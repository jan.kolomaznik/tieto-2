package tieto;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Cluster {

    public enum ClusterType {
        RED, GREEN;
    }

    // String of tieto.LogicalPort
    private ConcurrentMap<String, LogicalPort> logicalPorts = new ConcurrentHashMap<>();
    private ClusterType type;
    private String switch1, switch2;

    public Cluster(ClusterType type, String switch1, String switch2) {
        this.logicalPorts = logicalPorts;
        this.type = type;
        this.switch1 = switch1;
        this.switch2 = switch2;
    }

    public Cluster addLogicalPort(String String, LogicalPort logicalPort) {
        logicalPorts.put(String, logicalPort);
        return this;
    }

    public ConcurrentMap<String, LogicalPort> getLogicalPorts() {
        return logicalPorts;
    }

    public ClusterType getType() {
        return type;
    }

    public String getSwitch1() {
        return switch1;
    }

    public String getSwitch2() {
        return switch2;
    }

    @Override
    public String toString() {
        return "Cluster{" +
                ", type=" + type +
                ", switch1='" + switch1 + '\'' +
                ", switch2='" + switch2 + '\'' +
                '}';
    }
}
