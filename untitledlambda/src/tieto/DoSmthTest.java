package tieto;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class DoSmthTest {

    private static DoSmth doSmth;

    @BeforeClass
    public static void beforeClass() {
        doSmth = new DoSmth();
    }


    @Test
    public void retrieveOfflineLeafInterconnectPorts() throws NetworkingManagementException {
        // Given
        List<Cluster> clusters = new ArrayList<>();
        clusters.add(new Cluster(Cluster.ClusterType.GREEN, "swt1", "swt2")
                .addLogicalPort("lpG1", new LogicalPort("lpG1", LogicalPort.LogicalPortType.CLUSTER)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-a")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-b")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-c_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-d"))
                .addLogicalPort("lpG2", new LogicalPort("lpG2", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-a")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-b_null")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-c")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-d"))
                .addLogicalPort("lpG3", new LogicalPort("lpG3", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-a_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-b")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-c_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-d"))
        );
        clusters.add(new Cluster(Cluster.ClusterType.GREEN, "swt3", "swt4")
                .addLogicalPort("lpG4", new LogicalPort("lpG4", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG4-a")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG4-b")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG4-c_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG4-d"))
                .addLogicalPort("lpG5", new LogicalPort("lpG5", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG5-a")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG5-b_null")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG5-c")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG5-d_null"))
                .addLogicalPort("lpG6", new LogicalPort("lpG6", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG6-a_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG6-b")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG6-c_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG6-d")));
        // When
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            Map<String, List<String>> result = doSmth.retrieveOfflineLeafInterconnectPorts(clusters);
        }
        // Then
        System.out.printf("Duration %dms", System.currentTimeMillis() - start);
    }

    @Test
    public void retrieveOfflineLeafInterconnectPorts_lambda() throws NetworkingManagementException {
        // Given
        List<Cluster> clusters = new ArrayList<>();
        clusters.add(new Cluster(Cluster.ClusterType.GREEN, "swt1", "swt2")
                .addLogicalPort("lpG1", new LogicalPort("lpG1", LogicalPort.LogicalPortType.CLUSTER)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-a")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-b")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG1-c_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG1-d"))
                .addLogicalPort("lpG2", new LogicalPort("lpG2", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-a")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-b_null")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG2-c")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG2-d"))
                .addLogicalPort("lpG3", new LogicalPort("lpG3", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-a_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-b")
                        .addPhysicalSwitchPhysicalPort("swt1", "swt1-lpG3-c_null")
                        .addPhysicalSwitchPhysicalPort("swt2", "swt2-lpG3-d"))
        );

        clusters.add(new Cluster(Cluster.ClusterType.GREEN, "swt3", "swt4")
                .addLogicalPort("lpG4", new LogicalPort("lpG4", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG4-a")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG4-b")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG4-c_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG4-d"))
                .addLogicalPort("lpG5", new LogicalPort("lpG5", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG5-a")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG5-b_null")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG5-c")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG5-d_null"))
                .addLogicalPort("lpG6", new LogicalPort("lpG6", LogicalPort.LogicalPortType.LOOPBACK)
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG6-a_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG6-b")
                        .addPhysicalSwitchPhysicalPort("swt3", "swt3-lpG6-c_null")
                        .addPhysicalSwitchPhysicalPort("swt4", "swt4-lpG6-d")));

        // When
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
        Object result = doSmth.retrieveOfflineLeafInterconnectPorts_lambda(clusters);
        }
        // Then
        System.out.printf("Duration %dms", System.currentTimeMillis() - start);
    }
}