package tieto;

import javax.swing.text.html.parser.Entity;
import java.util.*;
import java.util.stream.Collectors;

import static tieto.LogicalPort.LogicalPortType.LOOPBACK;

public class DoSmth {
    // UUID of physicalSwitch - list of ports
    public Map<String, List<String>> retrieveOfflineLeafInterconnectPorts(Collection<Cluster> clusters)
            throws NetworkingManagementException {
        Map<String, List<String>> portsToDisable = new HashMap<>();

        for (Cluster cluster : clusters) {
            for (LogicalPort logicalPort : cluster
                    .getLogicalPorts().values().stream().filter(x -> LOOPBACK == x.getPortType())
                    .collect(Collectors.toList())) {
                for (Map.Entry<String, List<String>> entry : logicalPort.getPhysicalSwitchPhysicalPorts().entrySet()) {
                    for (String port : entry.getValue()) {
                        if (isPortOffline(entry.getKey(), port) == null) {
                            if (portsToDisable.get(entry.getKey()) == null) {
                                portsToDisable.put(entry.getKey(), new ArrayList<>());
                            }
                            portsToDisable.get(entry.getKey()).add(port);

                        }
                    }
                }
            }
        }
        return portsToDisable;
    }

    public static class SwithPort {

        private String swt;
        private String port;

        public SwithPort(String swt, String port) {
            this.swt = swt;
            this.port = port;
        }

        public String getSwt() {
            return swt;
        }

        public String getPort() {
            return port;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SwithPort swithPort = (SwithPort) o;
            return Objects.equals(swt, swithPort.swt) &&
                    Objects.equals(port, swithPort.port);
        }

        @Override
        public int hashCode() {
            return Objects.hash(swt, port);
        }

        @Override
        public String toString() {
            return "SwithPort{" +
                    "swt='" + swt + '\'' +
                    ", port='" + port + '\'' +
                    '}';
        }
    }


    public Object/*Map<String, List<String>>*/ retrieveOfflineLeafInterconnectPorts_lambda(Collection<Cluster> clusters)
            throws NetworkingManagementException {
        return clusters.stream()
                //.parallel()
                .filter(cluster -> cluster.getType() == Cluster.ClusterType.GREEN)
                .flatMap(cluster -> cluster.getLogicalPorts().values().stream())
                .filter(logicalPort -> logicalPort.getPortType() == LogicalPort.LogicalPortType.LOOPBACK)
                .flatMap(logicalPort -> logicalPort.getPhysicalSwitchPhysicalPorts().entrySet().stream())
                .flatMap(entry -> entry.getValue().stream()
                        //.parallel()
                        .filter(port -> isPortOffline(entry.getKey(), port) == null)
                        .map(port -> new SwithPort(entry.getKey(), port)))
                .collect(Collectors.groupingBy(
                        SwithPort::getSwt,
                        Collectors.mapping(
                                SwithPort::getPort, Collectors.toList()
                        )
                ));

    }

    private Object isPortOffline(String key, String port) {
        return port.endsWith("null") ? null : port;
    }
}
