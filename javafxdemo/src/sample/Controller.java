package sample;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    @FXML
    private Button button;

    @FXML
    private TextField text;

    @FXML
    private ListView list;

    private ObservableList<String> stringObservableList;

    // DAta
    private List<String> names = new ArrayList<>();


    public void click(MouseEvent event) {
        String newName = text.getText();
        stringObservableList.add(newName);
        text.clear();

        System.out.println(names);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        names.add("Honza");

        stringObservableList = new ObservableListWrapper<>(names);
        list.setItems(stringObservableList);
    }
}
