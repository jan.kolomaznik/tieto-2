package cz.ictpro.tieto;

import java.util.*;

public class Osoba implements Comparable<Osoba> {

    private String jmeno;
    private int rok;

    public Osoba(String jmeno, int rok) {
        this.jmeno = jmeno;
        this.rok = rok;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "jmeno='" + jmeno + '\'' +
                ", rok=" + rok +
                '}';
    }

    public static void main(String[] args) {
        List<Osoba> osoby = Arrays.asList(
                new Osoba("Honza", 15),
                new Osoba("Eva", 1),
                new Osoba("Jitka", 23),
                new Osoba("Honza", 32),
                new Osoba("Eva", 15),
                new Osoba("Jirka", 2),
                new Osoba("Jirka", 13));

        Collections.sort(osoby);
        osoby.forEach(System.out::println);

        Collections.sort(osoby, new Comparator<Osoba>() {
            @Override
            public int compare(Osoba o1, Osoba o2) {
                return (o1.jmeno != o2.jmeno) ? o1.jmeno.compareTo(o2.jmeno) : o1.rok - o2.rok;
            }
        });
        osoby.forEach(System.out::println);
    }

    public int compareTo(Osoba o) {
        return this.rok - o.rok;
    }
}
