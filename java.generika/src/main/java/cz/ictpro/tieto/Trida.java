package cz.ictpro.tieto;

class Trida {
    private Object prvekX;
    private Object prvekY;
    public Trida(Object valX, Object valY) {
        prvekX = valX;
        prvekY = valY;
    }
    public void setElements(Object valX, Object valY) {
        prvekX = valX;
        prvekY = valY;
    }
    public Object getElementX() {return prvekX;}
    public Object getElementY() {return prvekY;}

    public static void main(String[] args) {
        Trida t = new Trida("A", "B");
        Integer i = (Integer)t.getElementX();
        // Předchozí příkaz půjde zkompilovat,
        // ale za běhu způsobí výjimku ClassCastException.
    }
}
