package cz.ictpro.tieto;

import java.sql.SQLOutput;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Integer> data = Arrays.asList(1,2,3);
        List<String> ls = Arrays.asList("1", "2", "3");
        System.out.format("Suma: %f\n", suma(data));

        cast(Number.class,  2.2);

        //System.out.println(n.getClass());

        List<Number>dest = new ArrayList<>(0);

        //copy(dest, data);
        System.out.println(dest);
    }

    public static double suma(Collection<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

    public static <T extends Number> Set<T> singleton(T value) {
        final Set<T> result = new HashSet<>(1);
        result.add(value);
        return result;
    }

    public static <T, S extends T> void copy(List<T> dest, List<S> source) {
        for (S s: source) {
            //T t = cast(s);
            dest.add(s);
        }
    }

    public static <R, V extends R> R cast(Class<R> cls, V value) {
        System.out.println(cls.getCanonicalName());
        return value;
    }
}
