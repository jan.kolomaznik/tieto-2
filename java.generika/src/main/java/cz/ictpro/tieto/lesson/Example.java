package cz.ictpro.tieto.lesson;

public class Example {


    public static void main(String[] args) {
        // PUT api/nasedni/{carId = 1}/{zakaznikId = 2}
        Id<Auto> cadId = Id.ofAuto(1);
        Id<Zakaznik> zakaznikId = Id.ofZakaznik(2);
        nasedni(cadId, zakaznikId);
    }

    public static void nasedni(Id<Auto> carId, Id<Zakaznik> zakaznikId) {
        Zakaznik z = Zakaznik.getZakaznik(zakaznikId);
        Auto a = Auto.getAuto(carId);

        System.out.printf("Nasedl %s do auta %s", z.getName(), a.getName());
    }
}
