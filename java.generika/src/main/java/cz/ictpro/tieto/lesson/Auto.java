package cz.ictpro.tieto.lesson;

import java.util.HashMap;
import java.util.Map;

public class Auto {

    private static Map<Id<Auto>, Auto> repository;

    public static Auto getAuto(Id<Auto> id) {
        if (repository == null) {
            repository = new HashMap<>();
            repository.put(new Id<>(1), new Auto(1, "Osobni"));
            repository.put(new Id<>(2), new Auto(2, "Nakladni"));
        }
        return repository.get(id);
    }

    private long id;

    private String name;

    public Auto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
