package cz.ictpro.tieto.lesson;

import com.sun.nio.zipfs.ZipFileAttributes;

import java.util.Objects;

public class Id<T> {

    public static Id<Auto> ofAuto(long id) {
        return new Id<>(id, Auto.class);
    }

    public static Id<Zakaznik> ofZakaznik(long id) {
        return new Id<>(id, Zakaznik.class);
    }

    private final long value;
    private final Class<T> type;

    private Id(long value, Class<T> type) {
        this.value = value;
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public Class<T> getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Id<?> id = (Id<?>) o;
        return value == id.value &&
                Objects.equals(type, id.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, type);
    }

    @Override
    public String toString() {
        return "Id{" +
                "value=" + value +
                '}';
    }
}
