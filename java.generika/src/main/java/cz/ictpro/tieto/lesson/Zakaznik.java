package cz.ictpro.tieto.lesson;

import java.util.HashMap;
import java.util.Map;

public class Zakaznik {

    private static Map<Id<Zakaznik>, Zakaznik> repository;

    public static Zakaznik getZakaznik(Id<Zakaznik> id) {
        if (repository == null) {
            repository = new HashMap<>();
            repository.put(Id.ofZakaznik(1), new Zakaznik(1, "Pepa"));
            repository.put(Id.ofZakaznik(2), new Zakaznik(2, "Jana"));
        }
        return repository.get(id);
    }

    private long id;

    private String name;

    public Zakaznik(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
