package cz.ictpro.tieto;

class GenerickaTrida<T> {
    private T prvekX;
    private T prvekY;
    public GenerickaTrida(T valX, T valY) {
        prvekX = valX;
        prvekY = valY;
    }
    public void setElements(T valX, T valY) {
        prvekX = valX;
        prvekY = valY;
    }
    public T getElementX() {return prvekX;}
    public T getElementY() {return prvekY;}

    public static void main(String[] args) {
        GenerickaTrida<String> t = new GenerickaTrida<>("A", "2");
        Integer i = Integer.parseInt(t.getElementX());

        // Předchozí příkaz nepůjde zkompilovat
        // kvůli chybě "incorvetible types".
    }
}