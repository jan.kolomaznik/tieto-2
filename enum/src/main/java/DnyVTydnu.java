public enum DnyVTydnu {

    PO(true) {
        @Override
        public DnyVTydnu nextDay() {
            return null;
        }
    },
    UT(true) {
        @Override
        public DnyVTydnu nextDay() {
            return null;
        }
    },
    ST(true) {
        @Override
        public DnyVTydnu nextDay() {
            return null;
        }
    },
    CT(true) {
        @Override
        public DnyVTydnu nextDay() {
            return null;
        }
    },
    PA(true) {
        @Override
        public DnyVTydnu nextDay() {
            return SO;
        }
    },
    SO(false) {
        @Override
        public DnyVTydnu nextDay() {
            return NE;
        }
    },
    NE(false) {
        @Override
        public DnyVTydnu nextDay() {
            return PO;
        }
    };

    private final boolean pracovni;

    DnyVTydnu(boolean pracovni) {
        this.pracovni = pracovni;
    }

    public boolean isPracovni() {
        return pracovni;
    }

    public abstract DnyVTydnu nextDay();
}
