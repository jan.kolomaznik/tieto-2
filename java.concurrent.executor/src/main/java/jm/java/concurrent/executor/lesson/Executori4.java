package jm.java.concurrent.executor.lesson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Executori4 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        List<Callable<String>> callables = Arrays.asList(
                () -> "task1",
                () -> "task2",
                () -> "task3");

        List<Future<String>> futures = executor.invokeAll(callables);

        for (Future<String> future: futures) {
            System.out.println(future.get());
        }
    }
}
