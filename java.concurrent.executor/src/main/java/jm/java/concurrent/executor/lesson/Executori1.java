package jm.java.concurrent.executor.lesson;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executori1 {

    static class Task implements Runnable {

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
        }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 100; i++) {
            Task task = new Task();
            executor.submit(task);
        }
        System.out.println("Done");

    }
}
