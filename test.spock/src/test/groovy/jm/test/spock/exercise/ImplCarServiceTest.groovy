package jm.test.spock.exercise

import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class ImplCarServiceTest extends Specification {

    private ImplCarService service;

    private CarRepository carRepository;
    private RentService rentService;
    private Car car;

    def setup() {
        service = new ImplCarService();
        service.carRepository = carRepository = Mock(CarRepository)
        //car = new Car();
        //car.setName("Skodovka")
        //car.setId(5)
        car = new Car(id: 5, name: "Skodovka")
    }

    @Unroll
    def "Generate id #name -> #result"() {
        expect:
        service.createId(name) == result;

        where:
        name | result
        "aaa" | 3
        "bbbb" | 10
        "ccccc" | 5
    }

    def "Test set null to createID -> it should faild."() {
        when:
        int id = service.createId(null);
        then:
        assert id == 4;
    }

    def "Test cool metody"() {
        given:
        int a = 3;
        when:
        int result = service.coolMetoda(a)
        then:
        result == a * a
    }

    @Ignore
    def "Test more cool metody"() {
        given:
        int a = 3;
        when:
        int result = service.moreCoolMetoda(a)
        then:
        result == a * a
    }

    def "Save new Car"() {
        given:
        Car car = new Car(id: 1, name: "savedCar")

        when:
        Car savedCar = service.createCar("testCar")

        then:
        1 * carRepository.save(_) >> car
        savedCar == car
    }

}
