package jm.test.spock.exercise;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImplCarServiceTest2 {

    @Test
    void createId() {
        //setup:
        ImplCarService service = new ImplCarService();
        //when:
        int id = service.createId("pokus");
        //then:
        assertEquals(id, 4);
    }

    /* FIXME
    @Test
    void coolTest() {
        //given:
        ImplCarService service = new ImplCarService();
        int a = 3;
        //when:
        int result = service.moreCoolMetoda(a);
        //then:
        assert result == a * a;
    }
    */
}