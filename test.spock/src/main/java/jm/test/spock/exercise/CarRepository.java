package jm.test.spock.exercise;

import java.util.Optional;

public interface CarRepository {

    Car save(Car car);

    Optional<Car> findById(long carId);

    Iterable<Car> findAllCars();
}
