package jm.test.spock.exercise;

import java.util.Optional;

public class ImplCarService implements CarService {

    private CarRepository carRepository;

    private RentService rentService;

    @Override
    public Car createCar(String name) {
        Car car = new Car();
        car.setName(name);
        Car save = carRepository.save(car);
        return save;
    }

    @Override
    public Car rentCar(Car car, String user) {
        if (rentService.canRent(car, user)) {
            car.setRent(true);
            return carRepository.save(car);
        } else {
            throw new UnsupportedOperationException("Car cant by rent.");
        }
    }

    @Override
    public int createId(String name) {
        return name.length();
    }

    private int coolMetoda(int a) {
        return a * a * a;
    }

}
