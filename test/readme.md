[remark]:<class>(center, middle)
# Testy

[remark]:<slide>(new)
## Kdy testovat
Testování se určitě řadí mezi best practices. 

Názory na testování se různí:

[remark]:<slide>(wait)
1. **TDD** (Test driven development)

[remark]:<slide>(wait)
2. **EX** (Extrémní programování)

[remark]:<slide>(wait)
3. **Zdravý rozum** říká: některé praktiky bychom naopak neměli dodržovat až takto extrémně ...
   Co když potřebuji rychlý prototop???

[remark]:<slide>(wait)
#### Závěr: 
Testování je velmi důležité a v určité části projektu dokonce nepostradatelné. 

[remark]:<slide>(new)
## Rozšiřování softwaru
Jakékoli rozšiřování softwaru, pokud jej děláte kvalitně, vždy znamená změnu stávajících funkčností. 

[remark]:<slide>(wait)
Do jisté míry může kvalitní analýza a návrh připravit půdu pro budoucí úpravy, ale i když se budete hodně snažit, trh se mění nekontrolovaně a úspěšnou aplikaci bude třeba upravovat ze všech stran. 

[remark]:<slide>(wait)
Pokud stávající funkce nebudete upravovat, začne vám vznikat redundantní kód.

[remark]:<slide>(wait)
Asi víte, že vyhýbat se úpravě stávajícího kódu aplikace se vůbec nevyplatí, trpěl by tím její návrh a do budoucna by bylo velmi těžké takový slepenec nějak upravovat nebo rozšiřovat, když byste museli změny provádět na několika místech, bloky kódu by se opakovaly a podobně. 

[remark]:<slide>(wait)
Došli jsme tedy k tomu, že svou aplikaci budete stále měnit a přepisovat, takto vývoj softwaru prostě vypadá. 

[remark]:<slide>(wait)
A kdo potom otestuje, že vše funguje? Člověk? 

[remark]:<slide>(wait)
Když jsme došli k tomu, že musíme testovat, zda se předchozí funkčnost novou úpravou nerozbila, povězme si proč nemůže testování provádět člověk.

[remark]:<slide>(new)
### Proč to nemůže dělat člověk?

Zamysleme se nad naivní představou.

#### Očekávání
Programátor nebo tester si sedne k počítači a prokliká jednu službu za druhou, jestli fungují. 

#### Realita
[remark]:<slide>(wait)
Představte si, že takto testujete aplikaci, jste někde v polovině testovacího scénáře a naleznete chybu. 
- Nejde napsat komentář k článku 
- Chybu opravíte, úspěšně pokračujete až do konce scénáře. 
- Otestovanou aplikaci nasadíte na produkční server.

#### Co se stane?
[remark]:<slide>(wait)
- další den vám přijde email, že nejdou kupovat články. 
  Po chvíli zkoumání zjistíte, že oprava, kterou jste provedli, rozbila jinou funkčnost, kterou jste již otestovali předtím. 

[remark]:<slide>(new)
### Testy se musí provádět automaticky
Pokud se během testování provede oprava, musíte celý scénář provést od začátku! 

A právě proto musí testy provádět stroj, který celou aplikaci prokliká obvykle maximálně za desítky minut a může to dělat znovu a znovu a znovu. 

Proto píšeme automatické testy, toto vám bohužel většina návodů na internetu neřekne.


[remark]:<slide>(new)
## Typy testů
Zaměřme se nyní na to, co na aplikaci testujeme. 

Typů testů je hned několik, obvykle nepokrývají úplně všechny možné scénáře (všechen kód), ale hovoříme o code coverage. 

![](media/testovani_v_model.png)

[remark]:<slide>(new)
### Jednotkové testy (Unit testy)
Obvykle testují nejmenší jednotku kódu: objekt/metodu

Testujeme izolovaně, rychle, opakovatelně.

[remark]:<slide>(wait)
### Integrační testy
Právě integrační testy dohlíží na to, aby do sebe vše správně zapadalo.

[remark]:<slide>(wait)
### Akceptační testy
Tento typ testů je naopak úplně odstíněn od toho, jak je aplikace uvnitř naprogramovaná.

Testují funkconalitu celé aplikace pode useCase.

Tyto testy obvykle využívají framework Selenium, který umožňuje automaticky klikat ve webovém prohlížeči a simulovat internetového uživatele, co vaši aplikaci používá.

[remark]:<slide>(new)
### Systémové testy
I když aplikace funguje dobře, na produkčním prostředí podléhá dalším vlivům, se kterými musíme rovněž počítat, např. že by měla zvládat obsluhovat tisíc uživatelů v jeden okamžik. 
To bychom provedli zátěžovým testem, který spadá mezi systémové testy.

[remark]:<slide>(wait)
A mnohé další...

[remark]:<slide>(new)
## Pyramida testů
![](media/test-pyramid.jpeg)