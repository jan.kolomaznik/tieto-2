import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Servant {

    public static final Point NULL_POINT = new Point(0,0){

        @Override
        public void setX(int x) {

        }

        @Override
        public int getY() {
            return super.getY();
        }

        @Override
        public void setY(int y) {
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }
    };

    static class Point {

        private int x, y;
        private PropertyChangeSupport pcs;

        public Point(int x, int y) {
            this.pcs = new PropertyChangeSupport(this);
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            int oldx = this.x;
            this.x = x;
            this.pcs.firePropertyChange("X", oldx, x);
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            int oldY = this.y;
            this.y = y;
            this.pcs.firePropertyChange("Y", oldY, y);
        }

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            pcs.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            pcs.removePropertyChangeListener(listener);
        }
    }

    public static void main(String[] args) {
        Point point = new Point(1, 1);

        // Reagovat na udalost 6e se bod zmenil
        point.addPropertyChangeListener(evt -> System.out.println(evt));

        point.setX(2);
        point.setY(2);
        point.setX(3);

        NULL_POINT.setY(2);

    }

}
