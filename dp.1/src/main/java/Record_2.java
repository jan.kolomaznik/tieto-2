public class Record_2 {

    static class Name {
        private String name;

        public Name(String name) {
            this.name = name;
        }
    }

    static class NickName {
        private String nickName;

        public NickName(String nickName) {
            this.nickName = nickName;
        }
    }

    static class Phone {
        private String number;

        public Phone(String number) {
            this.number = number;
        }
    }

    private Name name;
    private NickName nickName;
    private Phone phone;

    public Record_2(Name name, NickName nickName, Phone phone) {
        this.name = name;
        this.nickName = nickName;
        this.phone = phone;
    }

    public static void main(String[] args) {
        Record_2 record = new Record_2(
                new Name("Jan"),
                new NickName("Honza"),
                new Phone("012 456 789"));
    }
}
