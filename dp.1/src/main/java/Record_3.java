public class Record_3 {

    static Name name(String name) {
        return new Name(name);
    }

    static NickName nickName(String nickName) {
        return new NickName(nickName);
    }

    static Phone phone(String number) {
        return new Phone(number);
    }

    static class Name {

        private String name;

        private Name(String name) {
            this.name = name;
        }
    }

    static class NickName {
        private String nickName;

        public NickName(String nickName) {
            this.nickName = nickName;
        }
    }

    static class Phone {
        private String number;

        public Phone(String number) {
            this.number = number;
        }
    }

    private Name name;
    private NickName nickName;
    private Phone phone;

    public Record_3(Name name, NickName nickName, Phone phone) {
        this.name = name;
        this.nickName = nickName;
        this.phone = phone;
    }

    public static void main(String[] args) {
        Record_3 record = new Record_3(
                name("Jan"),
                nickName("Honza"),
                phone("012 456 789"));
    }
}
