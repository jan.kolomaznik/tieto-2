public class Record {

    private String name;
    private String nickName;
    private String phone;

    public Record(String name, String nickName, String phone) {
        this.name = name;
        this.nickName = nickName;
        this.phone = phone;
    }

    public static void main(String[] args) {
        Record record = new Record("Jan", "Honza", "012 456 789");
    }
}
