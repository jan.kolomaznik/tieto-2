package jm.lombok.lesson;

import lombok.*;
import lombok.experimental.Wither;

import java.io.IOException;
import java.time.LocalDate;

public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        val person = new Person();
        var variable = new Person();

        try {
            person.foo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(new Person().toString());
    }

    protected boolean canEqual(Object other) throws IOException {
        return other instanceof Person;
    }

    @SneakyThrows
    public void foo() {
        canEqual(null);
    }

}
