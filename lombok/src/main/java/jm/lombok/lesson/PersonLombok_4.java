package jm.lombok.lesson;

import lombok.Value;

import java.time.LocalDate;

@Value
public class PersonLombok_4 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_4 person = new PersonLombok_4("Pepa", "Zdepa", null);
        System.out.println(person);
    }
}

